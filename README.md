# Search Hits Artist

SearchHitsArtist é um wrapper para [api do genius](https://docs.genius.com/) em que consiste em pegar os 10 hits do artista informado

## Instalação
Recomendo a utilização de algum ambiente virtual ([virtualenv](https://virtualenv.pypa.io/en/latest/installation/), [pyenv](https://github.com/pyenv/pyenv-installer), etc)

### virtualenv
```bash
virtualenv env --python python3.7
source env/bin/activate
pip install -r requirements.txt
```

Necessário declarar variavel de ambiente `TOKEN_GENIUS` com token de autenticação fornecido pela [genius](https://docs.genius.com/) (necessário se cadastrar no site)

## Rodando o server
```bash
python run.py
```

No navegador, basta acessar a seguinte url:
```
127.0.0.1:8000/artista/{nome-do-artista-desejado}
```

***evitar utilizar `/` no nome do artista***

Para algumas opções, execute:
```bash
python run.py --help
```

## Tests
```bash
python -m unittest app.tests
```
