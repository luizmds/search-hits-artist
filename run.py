import click
from app import  app

@click.command()
@click.option('--debug', is_flag=True, default=False,
              help='iniciar em modo debug')
@click.option('--port', type=int, default=8000,
              help='especifica a porta em que o server irá rodar')
def start_app(debug, port):
    app.run(host='0.0.0.0', port=port, debug=debug)



if __name__ == '__main__':
    start_app()