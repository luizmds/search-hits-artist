import json
import unittest
from . import app
import os
from unittest.mock import patch

class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app
        self.client = self.app.test_client()

    def test_get_top_10_hits(self):
        response = self.client.get('/acdc')
        hits = response.get_json()['hits']
        to_compare = [
            'Back in Black',
            'Highway to Hell',
            'Thunderstruck',
            'T.N.T.',
            'Hells Bells',
            'Dirty Deeds Done Dirt Cheap',
            'Shoot to Thrill',
            'You Shook Me All Night Long',
            'It’s a Long Way to the Top (If You Wanna Rock n’ Roll)',
            'Whole Lotta Rosie'
        ]
        self.assertListEqual(hits, to_compare)

    def test_get_empty_list(self):
        response = self.client.get('/nao-deve-existir-esse-cantor')
        hits = response.get_json()['hits']
        self.assertEqual(len(hits), 0)

    @patch('os.getenv')
    def test_forbbiden(self, mock_getenv):
        mock_getenv.return_value = 'forbbiden'
        response = self.client.get('/ac-dc')
        message = response.get_json()['message']
        self.assertEqual(message, 'não foi possivel realizar a requisição')
        self.assertEqual(response.status_code, 500)

