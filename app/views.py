import os
import requests
from flask_restful import Resource
from requests.exceptions import HTTPError, RequestException

URL_BASE = 'https://api.genius.com'
URL_SEARCH = URL_BASE + '/search'

FORBIDDEN = 403
UNAUTHORIZED = 401
INTERNAL_SERVER_ERROR = 500


def resquest_artist_search(name: str):
    params = {'q': name, 'access_token': os.getenv('TOKEN_GENIUS')}
    for _ in range(5):
        try:
            response = requests.get(URL_SEARCH, params=params)
            response.raise_for_status()
            return response
        except HTTPError as exc:
            status_code = exc.response.status_code
            if status_code in (FORBIDDEN, UNAUTHORIZED):
                raise
        except RequestException:
            continue
    else:
        raise


class ArtistHits(Resource):

    def get(self, name: str):
        try:
            response = resquest_artist_search(name)
        except RequestException:
            return ({'message': 'não foi possivel realizar a requisição'},
                    INTERNAL_SERVER_ERROR)
        hits = response.json()['response']['hits']
        return {'hits': [item['result']['title'] for item in hits]}
