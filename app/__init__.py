from flask import Flask
from flask_restful import Api

from .views import ArtistHits

app = Flask(__name__)
api = Api(app)

api.add_resource(ArtistHits, '/artista/<string:name>',
                 methods=["GET"])
